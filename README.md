A docker image for the [miniflux] RSS read based on Alpine.

# Usage

Copy `docker-compose.sample.yml` to `docker-compose.yml`, tweak it to your needs, then
run `docker-compose up -d`.

If you prefer to run standalone with sqlite instead of postgresql, you can use :

    $ docker run -d -v data-volume:/var/www/html/data -p 80:80 Shywim/miniflux

# The image in details

- Image based on richarvey/nginx-php-fpm
- PostgreSQL instead of SQLite
- Cronjob scheduled every hour to update feeds

