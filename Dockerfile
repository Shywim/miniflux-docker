FROM richarvey/nginx-php-fpm:1.3.9

ARG GIT_BRANCH="master"

# nginx-php-fpm env variables
ENV GIT_REPO="https://github.com/miniflux/miniflux-legacy" \
    GIT_BRANCH=${GIT_BRANCH}

RUN apk add --no-cache curl-dev postgresql-dev unzip && \
  docker-php-ext-install ctype \
   curl \
   iconv \
   mbstring \
   pdo_pgsql \
   session \
   xml

COPY conf /etc/

